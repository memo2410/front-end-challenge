import './style/style.scss'

document.addEventListener('DOMContentLoaded', function (event) {
  let data = null

  let addCard = (key, type, text, button, icon) => {
    let bgColor = ''
    let widthIcon = ''
    let heightIcon = ''

    let element = document.createElement('div')
    element.innerHTML = '<div class="card"><div class="container-text-card"><p>' + text + '</p><button class="button">' +
      button.replace(/\n/g, '<br />') + '</button></div><div class="container-icon-card"><img class="icon" src="./images/' + 
      icon + '.svg"></div></div>'

    while (element.firstChild) {
      type === 'play' ?
        document.getElementById('container-play').appendChild(element.firstChild) :
        document.getElementById('container-deposit').appendChild(element.firstChild)

      if (button === '') {
        document.getElementsByClassName('button')[key].style.display = 'none'
      }

      switch (icon) {
        case 'tick':
          bgColor = '#E9F2DF'
          widthIcon = '32'
          heightIcon = '25'
        break
        case 'clock':
          bgColor = '#FFECD6'
          widthIcon = '24'
          heightIcon = '23'
        break
        case 'lock':
          bgColor = '#7A64CC'
          widthIcon = '23'
          heightIcon = '36'
          document.getElementsByClassName('card')[key].style.opacity = '0.1'
        break
      }

      document.getElementsByClassName('container-icon-card')[key].style.backgroundColor = bgColor
      document.getElementsByClassName('icon')[key].style.width = widthIcon + 'px'
      document.getElementsByClassName('icon')[key].style.height = heightIcon + 'px'
    }
  }

  let xhr = new XMLHttpRequest();
  xhr.withCredentials = true;

  xhr.addEventListener('readystatechange', function () {
    if (this.readyState === 4) {
      let response = JSON.parse(xhr.responseText).data

      for (let i = 0; i < Object.keys(response).length; i++) {
        let key = Object.keys(response)[i]
        let value = response[key]
        addCard(key, value.type, value.text, value.button, value.icon)
      }
    }
  })

  xhr.open('GET', 'response.json')
  xhr.setRequestHeader('Accept', 'application/json')
  xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.send(data)
})
