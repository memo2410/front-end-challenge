require('webpack')
const WebpackBar = require('webpackbar')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const processHTMLPages = require('./processHTMLHelper.js')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const extractCSS = new ExtractTextPlugin('style.css')
const ProgressBar = new WebpackBar()
const plugins = [
  ProgressBar,
  extractCSS,

  new CopyWebpackPlugin([
      {
        from:'source/images',
        to:'images'
      }
  ]),
].concat(processHTMLPages())

module.exports = {
  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    './source/index.js'
  ],
  module: {
    rules: [
      {
        test: [/\.scss$/i, /\.sass$/i, /\.css$/],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'postcss-loader', 'sass-loader']
        })
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(jpe?g|gif|png|ttf|svg|json|woff|woff2|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader',
        query: {
          name: '[path][name].[ext]',
          context: './source'
        }
      }
    ],
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    },
    extensions: ['.js', '.es6']
  },
  output: {
    path: __dirname + '/build',
    filename: 'index.js'
  },
  devServer: {
    contentBase: './source'
  },
  plugins
}
