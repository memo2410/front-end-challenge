# Front-End Development Challenge

Technogi front-end development challenge


## Installation

The only dependency to start the project is to have installed. [Node.js](https://nodejs.org).

```sh
	npm install
```

## Features
	- Transpile ES6 to ES5
	- Compile SASS to CSS and add prefixes
	- Minify, concatenate and create JS and CSS bundles


## Developing

To start the [development server](http://localhost:8080) and the livereload of the development files located in the `source` folder, you should only run in the terminal

```sh
	npm run start
```

## Production

To compile the project for production runs

```sh
	npm run build
```
